from django.conf.urls import url
from . import views
urlpatterns = [
    url(r'^$',views.index,name='index'),
    #url(r'^files/', views.files, name='files'),
    #url(r'^purchase/', views.purchase, name='purchase'),
    #url(r'^detail/(\d+)/', views.detail, name='detail'),
    #url(r'^detail/report/(\d+)/', views.report, name='report'),
    #url(r'^order/', views.Order.as_view(), name = 'order'),
    #url(r'^orderdetail/(?P<pk>\d+)/$', views.OrderDetail.as_view(), name = 'orderdetail'),
    #url(r'^inform/', views.inform, name='inform'),
    #url(r'^informdetail/(?P<pk>\d+)/$', views.InformDetail.as_view(), name='informdetail'),
    #url(r'^balance/', views.balance, name='balance'),
    url(r'^upload/', views.upload, name='upload'),
    url(r'^bulk/', views.bulk, name='bulk'),
    #url(r'^export/csv/(?P<pk>\d+)/$', views.export, name='export'),
]
