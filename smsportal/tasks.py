# Create your tasks here
from __future__ import absolute_import, unicode_literals
from celery import shared_task
import requests
import json
from django.http import JsonResponse
import datetime
from rest_framework.parsers import JSONParser
import csv


@shared_task
def sendOrderTask(orderID, organizationID, organizationName, documentType, documentNumber, MSISDN, requestDate):
	url = 'http://104.198.66.40:11900/api/frontend/creditscore/order/customer/new'
	callback = 'https://portal.mobilescore.co.ke/score/inform/'
	state = 'NEW'
	data = {'orderID': str(orderID),
	'organizationID': organizationID,
	'organizationName': organizationName,
	'operation': state,
	'callbackURL': callback,
	'customer':{
	'documentType': documentType,
	'documentNumber': documentNumber,
	'MSISDN': MSISDN,
	'requestDate':requestDate,
	}}
	headers = {'Content-Type': 'application/json'}
	response = requests.post(url, data=json.dumps(data), headers=headers)
	#Add If Statements for Error Handling

	response_data = json.loads(response.text)
	if response.status_code == 200 and response_data['response_code']=="0":
	
		updateDateTime = str(datetime.date.today())
		status = "ACCEPTED"

		return updateOrderTask.delay(orderID, updateDateTime, status)
	else:
		updateDateTime = str(datetime.date.today())
		status = "CANCELLED"
		return updateOrderTask.delay(orderID, updateDateTime, status)

@shared_task
def updateOrderTask(OrderID, orderUpdateDateTime, orderStatus):
	url = 'https://portal.mobilescore.co.ke/score/orderdetail/'+ str(OrderID) + '/'
	data = {'order_udatetime': orderUpdateDateTime,
	'order_status': orderStatus,
	}
	headers = {'Content-Type': 'application/json'}
	response = requests.put(url, data=json.dumps(data), headers=headers)

	if response.status_code == 200:
		return JsonResponse({'response_code': '0', 'response_status': 'Success'})
	else:
		return JsonResponse({'response_code': 'E2', 'response_status': "Failed"})

@shared_task
def informOrderTask(payload):
	url = 'https://portal.mobilescore.co.ke/score/informdetail/'+ payload['id'] + '/'
	data = {'existence_index': payload['existence_index'],
	'defaulter_index': payload['defaulter_index'],
	'overdue_index': payload['overdue_index'],
	'aggregate_index': payload['aggregate_index'],
	'overall_score': payload['overall_score'],
	'subscriber_firstname': payload['subscriber_firstname'],
	'subscriber_lastname': payload['subscriber_lastname'],
	'scoring_data_id': payload['scoring_data_id'],
	'algorithm_id': payload['algorithm_id'],
	'order_udatetime': payload['order_udatetime'],
	'order_status': payload['order_status'],
	}
	headers = {'Content-Type': 'application/json'}
	response = requests.put(url, data=json.dumps(data), headers=headers)

	if response.status_code == 200:
		return JsonResponse({'response_code': '0', 'response_status': 'Success'})
	else:
		return JsonResponse({'response_code': 'E2', 'response_status': "Failed"})



@shared_task
def balanceQueryTask(payload):
	url = 'http://104.198.66.40:11900/api/frontend/creditscore/organization/balance/'
	data = {'organizationID': payload['organizationID'],
	'orderID': payload['orderID'],
	'operation': payload['operation'],
	}
	headers = {'Content-Type': 'application/json'}
	response = requests.post(url, data=json.dumps(data), headers=headers)
	responseObj = response.json()
	availableBalance = str(int(responseObj['balance']['availableBalance'])/100)

	if response.status_code == 200:
		return JsonResponse({'response_code': responseObj['response_code'],
			'response_status': responseObj['response_status'],
			'availableBalance': availableBalance})
	else:
		return JsonResponse({'response_code': 'E2', 'response_status': "Failed"})

@shared_task
def processFileTask(filepath, organization, batch):
	from score.models import OrganizationOrders001, Organization, Document
	#Set Organization instance
	organization = Organization.objects.get(id=organization)
	#Set Document instance
	document_batch = Document.objects.get(id=batch)

	with open(filepath, 'r') as f:
		reader = csv.reader(f.read().splitlines())
		next(reader)
		#data = [row for row in reader]

	Document.objects.filter(id=batch).update(processed='Running')

	for row in reader:
		mobile_number = int(row[0])
		document_type = row[1]
		document_number = row[2] 
		newOrder = OrganizationOrders001.objects.create(mobile_number = mobile_number,
		organization = organization,
		document_type = document_type,
		document_number = document_number,
		batch = document_batch)
		newOrder.save()

	Document.objects.filter(id=batch).update(processed='Completed')

	return 'Success'



