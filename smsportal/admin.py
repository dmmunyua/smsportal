# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from .models import Organization, OrganizationUserProfile, Document
# Register your models here.

class OrganizationAdmin(admin.ModelAdmin):
                list_display = ('organization_name', 'organization_type', 'organization_code', 'organization_krapin', 'organization_contactname', 'organization_contactnumber')

class OrganizationUserProfileAdmin(admin.ModelAdmin):
				list_display = ('user', 'organization', 'role')

#This will be deleted later---------------------------------------------------------------------------

class DocumentAdmin(admin.ModelAdmin):
				list_display = ('id','description', 'document', 'uploaded_at', 'processed', 'organization')


admin.site.register(Organization, OrganizationAdmin)
admin.site.register(OrganizationUserProfile, OrganizationUserProfileAdmin)
admin.site.register(Document, DocumentAdmin)

#This will be deleted later---------------------------------------------------------------------------

