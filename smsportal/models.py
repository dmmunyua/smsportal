# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.contrib.auth.models import User #added line to support User
from django.utils import timezone
from registration.signals import user_registered #added line to support User

from django.core.exceptions import ValidationError
from django.utils.deconstruct import deconstructible

from django.db.models.signals import post_save
from django.dispatch import receiver
#from score.tasks import sendOrderTask, processFileTask
import uuid, os, time, random, string
from django.conf import settings
from time import strftime
from uuid import uuid4



# Create your models here.

class Document(models.Model):

	def upload_csv_validator(upload_csv_obj):
		ext = os.path.splitext(upload_csv_obj.name)[1]  # [0] = returns path+filename
		valid_extension = ['.csv']
		if not ext in valid_extension:
			raise ValidationError(u'Unsupported file extension, .csv only.')
	@deconstructible
	class PathAndRename(object):
		def __init__(self, sub_path):
			self.path = sub_path

		def __call__(self, instance, filename):
			ext = filename.split('.')[-1]
			f_name = '-'.join(filename.replace('.csv', '').split() )
			rand_strings = ''.join( random.choice(string.lowercase+string.digits) for i in range(7) )
			#filename = '{}_{}{}.{}'.format(f_name, rand_strings, uuid4().hex, ext)
			filename = '{}_{}{}.{}'.format(f_name, rand_strings, time.strftime('%Y%m%d%H%M%S'), ext)

			return os.path.join(self.path, filename)
	PROCESS_OPTIONS = (
		('Completed', 'C'),
		('Running', 'R'),
		('Cancelled', 'N'),
		('Queued', 'Q'),
		)
	description = models.CharField(max_length=255, blank=True, default='Batch-'+time.strftime('%Y%m%d%H%M%S'))
	document = models.FileField(upload_to=PathAndRename('documents/'),
                                    validators=[upload_csv_validator]
                                    )
	uploaded_at = models.DateTimeField(auto_now_add=True)
	processed = models.CharField(max_length=10, choices=PROCESS_OPTIONS, default='Queued')
	download = models.TextField(blank=True)
	organization = models.ForeignKey("Organization", default=1)

	def __unicode__(self):
		return str(self.description)	

class Organization(models.Model):
	organization_name=models.CharField(max_length=200, unique=True)
	organization_type=models.CharField(max_length=50)
	organization_code=models.CharField(max_length=6, unique=True)
	organization_krapin=models.CharField(max_length=11)
	organization_contactname=models.CharField(max_length=200)
	organization_contactnumber=models.IntegerField()

	def __unicode__(self):
		return str(self.organization_name)

class OrganizationUserProfile(models.Model):
	user=models.OneToOneField(User, unique=True)
	organization=models.ForeignKey("Organization")
	role=models.CharField(max_length=50, blank=True, null=True)


	def __unicode__(self):
		return str(self.user)

'''   	
@receiver(post_save, sender=OrganizationOrders001)
def getOrderData(sender, instance, created, **kwargs):
	if created:
		orderID = instance.id
		MSISDN = instance.mobile_number
		documentType = instance.document_type
		documentNumber = instance.document_number
		requestDate =  instance.order_datetime
		for requester in Organization.objects.filter(organization_name=instance.organization):
			organizationID = requester.organization_code
			organizationName =  requester.organization_name
		if documentType == 'NATIONAL ID':
			convertDocumentType = 'National Id Card'
		else:
			convertDocumentType = documentType

		return sendOrderTask.delay(orderID, organizationID, organizationName, convertDocumentType, documentNumber, MSISDN, requestDate)

@receiver(post_save, sender=Document)
def getDocumentData(sender, instance, created, **kwargs):
	if created:
		filepath = os.path.join(settings.MEDIA_ROOT, str(instance.document))
		batch = instance.id
		for requester in Organization.objects.filter(organization_name=instance.organization):
			organization =  requester.id
		return processFileTask.delay(filepath, organization, batch)'''

