# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render, redirect
from .models import OrganizationUserProfile, Organization, Document

from rest_framework import mixins
from rest_framework import generics
from rest_framework.views import APIView
#from .serializers import OrderSerializer, OrderDetailSerializer, InformDetailSerializer
from rest_framework import permissions
#from score.tasks import informOrderTask, balanceQueryTask
from django.http import JsonResponse, HttpResponse
from django.views.decorators.csrf import csrf_exempt
from rest_framework.parsers import JSONParser
from weasyprint import HTML, CSS
from django.conf import settings
from django.template.loader import render_to_string
from django.core.files.storage import FileSystemStorage
from smsportal.forms import DocumentForm
import requests
import json
import tempfile
import os, csv


# Create your views here.
def index(request):
	if request.user.is_authenticated:
		#user_str = str(request.user)
		'''context = {
			#'organization_user': OrganizationUserProfile001.objects.filter(user=user_str).values('organization'),
			'total_orders': OrganizationOrders001.objects.filter(organization__organizationuserprofile001__user=request.user.pk).count(),
			'completed_orders': OrganizationOrders001.objects.filter(organization__organizationuserprofile001__user=request.user.pk, order_status='COMPLETED').count(),
		}'''
		return render(request, 'base.html')
	else:
		return render(request, 'base.html')



'''def files(request):
	if request.user.is_authenticated:
		
		context = {
			'organization_requester': Organization.objects.filter(organizationuserprofile__user=request.user.pk),
		}
		return render(request, 'files.html', context)
	else:
		return render(request, 'base.html')'''


def bulk(request):
	if request.user.is_authenticated:
		
		context = {
			'uploaded_files': Document.objects.filter(organization__organizationuserprofile__user=request.user.pk),
			'organization_requester': Organization.objects.filter(organizationuserprofile__user=request.user.pk),
		}
		return render(request, 'bulk.html', context)
	else:
		return render(request, 'base.html')
'''	
def purchase(request):
	if request.user.is_authenticated:
		
		organization_requesting = Organization.objects.filter(organizationuserprofile001__user=request.user.pk)
		context = {
			'prices': Tariff.objects.filter(id__in=organization_requesting)
		}
		return render(request, 'purchase.html', context)
	else:
		return render(request, 'base2.html')

def detail(request, organizationorders001_id):
	if request.user.is_authenticated:
		context = {
			'details': OrganizationOrders001.objects.get(id=organizationorders001_id)
		}
		
		return render(request, 'detail.html', context)
	else:
		return render(request, 'base2.html')
'''

'''
def report(request, organizationorders001_id):
	context = {
		'prints': OrganizationOrders001.objects.get(id=organizationorders001_id)
	}

	report_css = os.path.join(os.path.dirname(__file__), "static", "css", "report.css")

	html_string = render_to_string('report.html', context)
	html = HTML(string=html_string)
	result = html.write_pdf(stylesheets=[report_css])

	response = HttpResponse(content_type='application/pdf;')
	response['Content-Disposition'] = 'inline; filename=report_'+ str(context.get("prints").mobile_number) + '_' + organizationorders001_id +'.pdf'
	response['Content-Transfer-Encoding'] = 'binary'
	with tempfile.NamedTemporaryFile(delete=True) as output:
		output.write(result)
		output.flush()
		output = open(output.name, 'r')
		response.write(output.read())

	return response
	'''

'''
class Order(generics.ListCreateAPIView):
    queryset = OrganizationOrders001.objects.all()
    serializer_class = OrderSerializer


class OrderDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = OrganizationOrders001.objects.all()
    serializer_class = OrderDetailSerializer

class InformDetail(generics.RetrieveUpdateDestroyAPIView):
	queryset = OrganizationOrders001.objects.all()
	serializer_class = InformDetailSerializer
'''


def upload(request):
	if request.method == 'POST':
		form = DocumentForm(request.POST, request.FILES, prefix='document')
		if form.is_valid():
			form.save()
			return redirect('bulk')
		else:
			form = DocumentForm()
			return redirect('index')

'''
@csrf_exempt
def inform(request):
	if request.method == 'POST':
		data = JSONParser().parse(request)
		informOrderTask.delay(data)
		return JsonResponse({'response_code': '0', 'response_status': 'Success'})
	else:
		return JsonResponse({'response_code': 'E1','response_status': 'Invalid Request'})'''

'''

def balance(request):
	if request.method == 'POST':
		rData = JSONParser().parse(request)
		codes = Organization.objects.filter(organizationuserprofile001__user=request.user.pk)
		for code in codes:
			organizationID = code.organization_code
		data = {'organizationID': organizationID,
		'orderID': rData['orderID'],
		'operation': rData['operation'],
		}

		response = balanceQueryTask.delay(data).get()
		return response
	else:
		return JsonResponse({'response_code': 'E1','response_status': 'Invalid Request'})


'''
'''
def export(request, pk):
    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = 'attachment; filename="Batch.csv"'

    writer = csv.writer(response)
    writer.writerow(['Mobile Number',
    	'Existence Index',
    	'Defaulter Index',
    	'Overdue Index',
    	'Aggregate Index',
    	'Overall Score',
    	'Algorithm Used',
    	'Create Date',
    	'Update Date',
    	'Order Status'])

    orders = OrganizationOrders001.objects.filter(batch=pk).values_list('mobile_number',
    	'existence_index',
    	'defaulter_index',
    	'overdue_index',
    	'aggregate_index',
    	'overall_score',
    	'algorithm_id',
    	'order_datetime',
    	'order_udatetime',
    	'order_status')
    for order in orders:
        writer.writerow(order)

    return response '''
