from registration.forms import RegistrationForm
from django import forms
from .models import Document, Organization
 
class ExRegistrationForm(RegistrationForm):
    organization_name = forms.ModelChoiceField(queryset=Organization.objects.all(), label = "Organization Name")
    #role=forms.CharField(label = "Role")

class DocumentForm(forms.ModelForm):
	class Meta:
		model = Document
		fields = ('organization','document', )